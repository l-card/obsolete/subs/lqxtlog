#ifndef LQXTTABLELOGGERENGINE_H
#define LQXTTABLELOGGERENGINE_H

#include <QxtLoggerEngine>
#include <QObject>
#include <QDateTime>
#include <QPoint>
#include <QColor>

class QPushButton;
class QAction;
class QMenu;
class QFrame;
class QTableWidget;

class LQxtTableLoggerEngine : public QObject, public QxtLoggerEngine
{
    Q_OBJECT
public:
    explicit LQxtTableLoggerEngine(QxtLogger::LogLevel lvl = QxtLogger::InfoLevel,
                                   QObject* parent=0, bool clear_btn=false);

    void    initLoggerEngine();


    void    killLoggerEngine();
    bool    isInitialized() const;

    void    writeFormatted(QxtLogger::LogLevel level, const QList<QVariant>& messages);


    QFrame* widget() {return m_wgt;}

    QxtLogger::LogLevel logLevel() const {return m_lvl;}

public slots:
    void clear(void);
    void setLogLevel(QxtLogger::LogLevel lvl);
private:
    class logEntry
    {
    public:
        logEntry(QDateTime time, QxtLogger::LogLevel lvl,QString msg) :
            m_time(time), m_lvl(lvl), m_msg(msg) {}
        logEntry() {logEntry(QDateTime::currentDateTime(), QxtLogger::TraceLevel, "");}

        QDateTime time() const {return m_time;}
        QxtLogger::LogLevel level() const {return m_lvl;}
        QString message() const {return m_msg;}

    private:
        QDateTime m_time;
        QxtLogger::LogLevel m_lvl;
        QString m_msg;
    };

    class logLvlCfg
    {
    public:
        logLvlCfg(QxtLogger::LogLevel lvl, QColor color, QString text) :
            m_lvl(lvl), m_color(color), m_text(text) {}

        QxtLogger::LogLevel level() const {return m_lvl;}
        QString name() const {return m_text;}
        QColor color() const {return m_color;}
    private:
        QxtLogger::LogLevel m_lvl;
        QColor m_color;
        QString m_text;
    } ;


    void showLogEntry(logEntry& entry);
    logLvlCfg getLvlCfg(QxtLogger::LogLevel level);

    QPushButton* m_clearButton;
    QTableWidget* m_logbox;
    QAction* m_actClear;
    QMenu* m_contextMenu;
    QFrame* m_wgt;

    QHash<QAction*, int> m_logLvlActMap;

    QList<logEntry> m_logEntries;
    QxtLogger::LogLevel m_lvl;
    QList<logLvlCfg> m_logLvlCfg;



private slots:
    void showContextMenu(QPoint point);
    void lvlActTriggered();
};

#endif // LQXTTABLELOGGERENGINE_H
