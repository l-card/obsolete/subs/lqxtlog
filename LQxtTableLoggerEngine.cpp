#include "LQxtTableLoggerEngine.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QScrollBar>
#include "LQTableWidget.h"








LQxtTableLoggerEngine::LQxtTableLoggerEngine(QxtLogger::LogLevel lvl, QObject *parent, bool clear_btn) :
    QObject(parent), QxtLoggerEngine(), m_lvl(lvl)
{

    m_logLvlCfg.append(logLvlCfg(QxtLogger::TraceLevel, Qt::gray, QObject::tr("Трассировка")));
    m_logLvlCfg.append(logLvlCfg(QxtLogger::DebugLevel, Qt::black, QString("Отладка")));
    m_logLvlCfg.append(logLvlCfg(QxtLogger::InfoLevel,  Qt::darkGreen, QString("Информация")));
    m_logLvlCfg.append(logLvlCfg(QxtLogger::WarningLevel, Qt::darkYellow, QString("Предупреждения")));
    m_logLvlCfg.append(logLvlCfg(QxtLogger::ErrorLevel, Qt::red, QString("Ошибки")));
    m_logLvlCfg.append(logLvlCfg(QxtLogger::CriticalLevel, Qt::darkRed, QString("Критические ошибки")));


    m_wgt = new QFrame();

    m_wgt->setLineWidth(0);

    m_wgt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout* vLayout = new QVBoxLayout(); //вертикальное размещение - гориз. + список
    QHBoxLayout* hLayout = new QHBoxLayout(); //горизонтальное размещение - для кнопок

    vLayout->setMargin(0);
    vLayout->setSpacing(0);

    hLayout->setMargin(0);
    hLayout->setSpacing(0);


    hLayout->addStretch(3);
    if (clear_btn)
    {
        int btn_h = 17;
        m_clearButton = new QPushButton("X");
        hLayout->addWidget(m_clearButton);
        m_clearButton->setFixedHeight(btn_h);
        connect(m_clearButton, SIGNAL(clicked(bool)), this, SLOT(clear()));
    }

    vLayout->addLayout(hLayout);

    m_logbox = new LQTableWidget(0,2);
    m_logbox->horizontalHeader()->setVisible(false);
    m_logbox->verticalHeader()->setVisible(false);
    m_logbox->horizontalHeader()->setStretchLastSection(true);
    m_logbox->setColumnWidth(0, 120);
    m_logbox->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_logbox->setSelectionBehavior(QAbstractItemView::SelectRows);


    vLayout->addWidget(m_logbox);

    m_wgt->setLayout(vLayout);

    m_actClear = new QAction(tr("Очистить"), this);
    m_actClear->setIcon(m_wgt->style()->standardIcon(QStyle::SP_DialogResetButton));
    connect(m_actClear, SIGNAL(triggered()), SLOT(clear()));

    //warn_icon.load(QCoreApplication::applicationDirPath() + "/icons/warning.jpg");
    //err_icon.load(QCoreApplication::applicationDirPath() + "/icons/error.jpg");

    m_logbox->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_logbox, SIGNAL(customContextMenuRequested(QPoint)), SLOT(showContextMenu(QPoint)));

    m_contextMenu = new QMenu(m_wgt);
    m_contextMenu->addAction(m_actClear);

    QMenu* menuLvl = new QMenu("Уровень лога");

    for (int i=0; i<m_logLvlCfg.size(); i++)
    {
        QAction* act = new QAction(m_logLvlCfg[i].name(), this);
        act->setCheckable(true);
        if (m_logLvlCfg[i].level()==m_lvl)
            act->setChecked(true);
        m_logLvlActMap[act] = i;
        menuLvl->addAction(act);
        connect(act, SIGNAL(triggered()), SLOT(lvlActTriggered()));
    }

    m_contextMenu->addMenu(menuLvl);
}

void LQxtTableLoggerEngine::initLoggerEngine()
{
}

void LQxtTableLoggerEngine::killLoggerEngine()
{
}


bool LQxtTableLoggerEngine::isInitialized() const
{
    return true;
}

void LQxtTableLoggerEngine::writeFormatted(QxtLogger::LogLevel level, const QList<QVariant> &messages)
{
    QString msg;
    foreach (QVariant var, messages)
    {
        msg.append(var.toString());
    }



    logEntry entry(QDateTime::currentDateTime(), level, msg);
    m_logEntries.append(entry);
    showLogEntry(entry);
}

void LQxtTableLoggerEngine::clear()
{
    m_logEntries.clear();
    m_logbox->setRowCount(0);
}

void LQxtTableLoggerEngine::setLogLevel(QxtLogger::LogLevel lvl)
{
    m_lvl = lvl;
    foreach (QAction* act, m_logLvlActMap.keys())
    {
        if (m_logLvlCfg[m_logLvlActMap[act]].level()==lvl)
            act->setChecked(true);
        else
            act->setChecked(false);
    }

    m_logbox->setRowCount(0);
    foreach (logEntry entry, m_logEntries)
    {
        showLogEntry(entry);
    }
}

void LQxtTableLoggerEngine::showLogEntry(LQxtTableLoggerEngine::logEntry &entry)
{
    if (entry.level() >= m_lvl)
    {
        logLvlCfg cfg = getLvlCfg(entry.level());

        QScrollBar* bar = m_logbox->verticalScrollBar();
        bool at_end = bar->value() == bar->maximum();

        int row_cnt = m_logbox->rowCount();
        m_logbox->setRowCount(row_cnt+1);

        QTableWidgetItem* item = new QTableWidgetItem(entry.message());
        item->setToolTip(item->text());



        item->setForeground(QBrush(cfg.color()));

        QTableWidgetItem* timeItem = new QTableWidgetItem(entry.time().toString("d.M.yyyy, h:mm:ss"));

        timeItem->setForeground(QBrush(cfg.color()));

        m_logbox->setItem(row_cnt, 0, timeItem);
        m_logbox->setItem(row_cnt, 1, item);
        m_logbox->resizeRowToContents(row_cnt);


        if (at_end)
            m_logbox->scrollToBottom();
    }
}

LQxtTableLoggerEngine::logLvlCfg LQxtTableLoggerEngine::getLvlCfg(QxtLogger::LogLevel level)
{
    for (int i=0; i<m_logLvlCfg.size(); i++)
    {
         if (m_logLvlCfg[i].level() == level)
            return m_logLvlCfg[i];
    }
    return m_logLvlCfg.last();
}

void LQxtTableLoggerEngine::showContextMenu(QPoint point)
{
    m_contextMenu->popup(m_logbox->mapToGlobal(point));
}

void LQxtTableLoggerEngine::lvlActTriggered()
{
    QAction* trig_act = qobject_cast<QAction*>(sender());
    if (trig_act)
        setLogLevel(m_logLvlCfg[m_logLvlActMap[trig_act]].level());
}
